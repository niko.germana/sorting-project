package com.niko;

public class SumCalculator {

    public int calculateSum(String[] args) {
        int sum = 0;
        for (String arg : args) {
            try {
                sum += Integer.parseInt(arg);
            } catch (NumberFormatException e) {}
        }
        return sum;
    }

    public static void main(String[] args) {
        SumCalculator calculator = new SumCalculator();
        int result = calculator.calculateSum(args);
        System.out.println("Sum of provided numbers: " + result);
    }
}