package com.niko.test;

import com.niko.SumCalculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SumCalculatorTest {
    private final String[] inputArgs;
    private final int expectedResult;

    public SumCalculatorTest(String[] inputArgs, int expectedResult) {
        this.inputArgs = inputArgs;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters(name = "{index}: sum({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(
                new Object[][]{
                        {new String[]{}, 0}, // zero
                        {new String[]{"5"}, 5}, // one
                        {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, 55}, // ten
                        {new String[]{"10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "200"}, 750} // more than ten
                }
        );
    }

    @Test
    public void testCalculateSum() {
        SumCalculator calculator = new SumCalculator();
        int result = calculator.calculateSum(inputArgs);
        assertEquals(expectedResult, result);
    }
}
